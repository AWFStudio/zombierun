﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//на персонаже висит 2 коллайдера: коробочка 2D и круглый 2D
//круглый коллайдер нужен, чтобы перс мог ходить по наклонным поверхностям
//коллайдеры должны представлять собой "матрешку" (одно целое), чтобы персонаж не цеплялся нижними углами бокс-коллайдера ни за что
//на перса также вешаем ригидбоди 2D и замораживаем вращение по z, ставим гравитацию 20, включаем интерполяцию (для плавности падения)
//заходим в "edit", "project settings", "physics 2D" и выставляем гравитацию по y на -30


public class CharacterMotion : MonoBehaviour
{
    CharacterController2D controller;

    public float speed = 20f;
    public float horizontalMotion = 0f;

    bool jump = false;
    bool crouch = false;

    void Awake()
    {
        controller = GetComponent<CharacterController2D>();
    }

    void Update()
    {
        //персонаж идет
        horizontalMotion = Input.GetAxisRaw("Horizontal") * speed;
        Debug.Log(horizontalMotion);
    }
    void FixedUpdate()
    {
        //персонаж ходит, прыгает и ползает с одной и той же скоростью
        controller.Move(horizontalMotion * Time.fixedDeltaTime, crouch, jump);
    }
}
