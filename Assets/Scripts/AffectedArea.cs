﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//зона поражения

public class AffectedArea : MonoBehaviour
{
    [SerializeField] GameObject respawnPoint;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Character")
        {
            //если игрок навернулся сюда, переносим его на точку респа
            other.transform.position = respawnPoint.transform.position;
        }
    }
}
